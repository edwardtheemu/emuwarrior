﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EmuWarrior.Data
{
    internal static class Enums
    {
        public enum ResourceType
        {
            Rage,
            Energy,
            Mana
        }

        public enum WarriorStance
        {
            Battle = 1,
            Defensive,
            Berserker
        }
    }
}
